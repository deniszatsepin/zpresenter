module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        laxcomma: true,
        node: true,
        devel: true,
        globals: {
          jQuery: true
        },
      },

      all: ['Gruntfile.js', 'src/**/*.js']
    },
    // Configure a mochaTest task
    mochaTest: {
      test: {
        options: {
          reporter: 'spec'
        },
        src: ['test/**/*.js']
      }
    },

    copy: {
      main: {
        files: [
          {expand: true, cwd: 'bower_components/jquery/', src: ['jquery.js'], dest: 'js/lib/'},
          {expand: true, cwd: 'src/', src: ['*.js'], dest: 'js/'}
        ]
      }
    }
  });

  // Add the tasks here.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('hint', 'jshint');
  grunt.registerTask('test', ['mochaTest']);

  grunt.registerTask('default', ['hint', 'test', 'copy']);
};
