;(function(window, $) {
	
	var SlideChanger = {
		simple: function (firstSlide, secondSlide, back) {
			var $first = $(firstSlide);
			var $second = $(secondSlide);
			var width = this.$root.width();

			if (back) {
				$second.insertBefore($first);
				$first.css('top', '0px').css('left', '0px');
				$second.css('top', '0px').css('left', '-' + width + 'px');
			} else {
				$second.insertAfter($first);
				$first.css('top', '0px').css('left', '0px');
				$second.css('top', '0px').css('left', width + 'px');
			}

			$first.css('display','block');
			$second.css('display', 'block');
			if (back) {
				$first.animate({
					left: '+=' + width	
				});

				$second.animate({
					left: '+=' + width	
				}, {
					complete: $.proxy(function ($first) {
						this.animating = false;
						$first.css('display', 'none');
					}, this, $first)
				});
			} else {
				$first.animate({
					left: '-=' + width	
				});

				$second.animate({
					left: '-=' + width	
				}, {
					complete: $.proxy(function ($first) {
						this.animating = false;
						$first.css('display', 'none');
					}, this, $first)
				});
			}

			
		},

		fade: function (firstSlide, secondSlide, back) {
			var $first = $(firstSlide);
			var $second = $(secondSlide);
			var width = this.$root.width();
			var that = this;

			$first.css('top', '0px').css('left', '0px').css('z-index', '10');
			$second.css('top', '0px').css('left', '0px').css('z-index', '9').css('opacity', '0');

			$first.css('display','block');
			$second.css('display', 'block');
			
			$first.animate({opacity: 0});
			$second.animate({opacity: 1}, {
				complete: $.proxy(function ($first) {
					this.animating = false;
					$first.css('display', 'none');
				}, this, $first)
			});

		}
	};


	var Presenter = (function(){
		var Presenter = function() {
			this.init.apply(this, arguments);
		};

		Presenter.prototype.init = function (container, settings) {
			this.root = container;
			this.$root = $(container);
			this.settings = settings;
			this.slides = $('section', container);
			this.commands = [];
			this.max = 20;
			this.animating = false;

			this.nextButton = settings.nextButton || this.root;
			this.prevButton = settings.prevButton || null;
			this.playButton = settings.playButton || null;


			this.$root.addClass('z-presenter-container');
			this.current = 0;
			for (var i = 0, len = this.slides.length; i < len; i += 1) {
				if ( i > 0 ) {
					$(this.slides[i]).css('display', 'none');
				}
			}

			this.loopId = setInterval($.proxy(function() {
				this.loop();
			}, this), 100);

			this.setHandlers();
		};

		Presenter.prototype.getNext = function () {
			return this.current === this.slides.length - 1 ? 0 : this.current + 1;
		}; 

		Presenter.prototype.getPrev = function () {
			return this.current === 0 ? this.slides.length - 1 : this.current - 1;
		};

		Presenter.prototype.nextSlide = function (sNr) {
			var current = this.current;
			var next = sNr || this.getNext();
			if (current === next) {
				return;
			}
			SlideChanger[this.settings.slide].call(this, this.slides[current], this.slides[next]);
			this.current = next;
		};

		Presenter.prototype.prevSlide = function (sNr) {
			var current = this.current;
			var prev = sNr || this.getPrev();
			if (current === prev) {
				return;
			}
			SlideChanger[this.settings.slide].call(this, this.slides[current], this.slides[prev], true);
			this.current = prev;
		};

		Presenter.prototype.addCommand = function (command) {
			if (this.commands.length < this.max) {
				this.commands.push(command);
			}
		};

		Presenter.prototype.loop = function () {
			if (this.animating) {
				return;
			}

			if (this.commands.length) {
				var command = this.commands.shift();

				switch(command) {
					case 'next':
						this.nextSlide();
						this.animating = true;
						break;
					case 'prev':
						this.prevSlide();
						this.animating = true;
						break;
					default:
				}
			}
		};

		Presenter.prototype.setHandlers = function () {
			if (this.nextButton) {
				$(this.nextButton).on('click', $.proxy(function(e){
					this.addCommand('next');
				}, this));
			}

			if (this.prevButton) {
				$(this.prevButton).on('click', $.proxy(function(e){
					this.addCommand('prev');
				}, this));
			}
		};


		return Presenter;
	})();



	$.fn.zpresenter = function (options) {
		
		var defaults = {
			controls: 'bottom',
			slide: 'simple'
		};

		var settings = $.extend({}, defaults, options);

		return this.each(function () {
			console.log('args: ', arguments);
			$(this).zpresenter = new Presenter(this, settings);
			return this;
		});
	};
})(window, window.jQuery);