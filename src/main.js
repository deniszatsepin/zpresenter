;(function(window, $){
	$(function(){
		$(".p1").zpresenter({
			controls: 'top',
			nextButton: $('.p1 .control .nextButton'),
			prevButton: $('.p1 .control .prevButton')
		});

		$(".p3").zpresenter({
			controls: 'top',
			slide: 'fade',
			nextButton: $('.p3 .control .nextButton'),
			prevButton: $('.p3 .control .prevButton')
		});

		$(".p2").zpresenter({
			controls: 'top'	
		});

	});
})(window, window.jQuery);